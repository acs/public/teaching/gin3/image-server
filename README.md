# GIN3-Image for VirtualBox

This repository contains an VM image for VirtualBox.
The Image is used for the course "Fundamentals of Computer Science 3 - Operating Systems and System Security" and can be downloaded by following command.

```
$ curl -O https://git.rwth-aachen.de/acs/public/teaching/gin3/image-server/-/raw/main/vm_image.tar.gz
```
